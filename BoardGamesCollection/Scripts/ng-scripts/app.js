﻿(function () {
    var app = angular.module('app', []);

    app.service('communicationService', function () {
        var self = this;
        this.game = {};


    });
    //used this and self instead of scope so properties can be called inside other controllers in the html using the alias
    app.controller('GamesController', [
        '$http', '$rootScope', 'communicationService', function ($http, $rootScope, communicationService) {
            var self = this;
            //self.showEdit = false;
            self.show = 1; // 1 - List, 2 - Add, 3 - Edit, 4- Delete
            self.games = [];
            $http.get('/api/GamesApi').success(function (data) {
                self.games = data;
            });

            self.games = self.games;

            self.edit = function (game) {
                self.show = 3;
                communicationService.game = game;
                $rootScope.$broadcast('updateClicked');
                /*I also though about sending the update controller to the service directly.
                Then having a function in the service that used the saved controller to call a function
                in said controller (update controller) that updated the values.
                Decided this looked much cleaner.*/
            };

            self.delete = function(game) {
                self.show = 4;
                communicationService.game = game;
                $rootScope.$broadcast('deleteClicked');
            };
        }
    ]);

    app.controller('AddGameController', [
        '$http' ,function ($http) {
            var self = this;
            this.game = {};
            this.id = 0;

            this.add = function (games) {
                $http.post('/api/GamesApi', self.game).success(function (data) {
                    self.game.GameId = data;
                    var newGame = {};
                    newGame.GameId = self.game.GameId;
                    newGame.GameName = self.game.GameName;
                    newGame.GameDescription = self.game.GameDescription;

                    games.push(newGame);
                    self.game = {};
                }).error(function () {
                    console.log('Couldn\'t add new Game');
                });
            }
        }
    ]);
    app.controller('UpdateGameController', [
        '$http', '$scope', 'communicationService', function ($http, $scope, communicationService) {
            var self = this;
            this.game = {};
            this.originalGame = {};

            $scope.$on('updateClicked', function(event, data) {
                self.originalGame = communicationService.game;
                self.game.GameId = self.originalGame.GameId;
                self.game.GameName = self.originalGame.GameName;
                self.game.GameDescription = self.originalGame.GameDescription;
            });
            
            this.update = function () {
                $http.post('/api/GamesApi', self.game).success(function () {
                    self.originalGame.GameName = self.game.GameName;
                    self.originalGame.GameDescription = self.game.GameDescription;
                }).error(function () {
                    console.log('Couldn\'t update Game');
                });
            }
            
        }
    ]);

    app.controller('DeleteGameController', [
        '$http', '$scope', 'communicationService', function($http, $scope, communicationService) {
            var self = this;
            this.game = {}

            $scope.$on('deleteClicked', function (event, data) {
                self.game = communicationService.game;
            });

            this.delete = function (games) {
                $http.delete('/api/GamesApi/'+self.game.GameId).success(function () {
                    var index = games.indexOf(self.game);
                    games.splice(index, 1);
                }).error(function () {
                    console.log('Couldn\'t delete Game');
                });
            }


        }
    ]);


})();