﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoardGamesCollection.Providers;
using BoardGamesCollection.ValueObjects;

namespace BoardGamesCollection.Controllers
{
    public class GamesApiController : ApiController
    {
        private readonly IGameProvider _gameProvider;

        public GamesApiController(IGameProvider gameProvider)
        {
            _gameProvider = gameProvider;
        }


        // GET api/<controller>
        public IEnumerable<Game> Get()
        {
            var games = _gameProvider.GetAllGames();
            return games ?? new List<Game>();
        }

        // GET api/<controller>/5
        public Game Get(int id)
        {
            var game = _gameProvider.GetGameById(id);
            return game;
        }

        // POST api/<controller>
        public int Post(Game game)
        {
            if (game.GameId == 0)
            {
                _gameProvider.AddGame(game);
            }
            else
            {
                _gameProvider.UpdateGame(game);
            }
            return game.GameId;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _gameProvider.DeleteGame(id);
        }
    }
}