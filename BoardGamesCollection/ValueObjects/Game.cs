﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesCollection.ValueObjects
{
    public class Game
    {
        public int GameId { get; set; }
        public string GameName { get; set; }
        public string GameDescription { get; set; }
    }
}