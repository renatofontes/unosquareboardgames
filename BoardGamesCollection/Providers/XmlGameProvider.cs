﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using BoardGamesCollection.ValueObjects;

namespace BoardGamesCollection.Providers
{
    public class XmlGameProvider : IGameProvider
    {
        private const string DirectoryPath = @"C:\BoardGames";
        private const string XmlPath = @"C:\BoardGames\XmlDataFile.xml";

        public int AddGame(Game game)
        {
            //Get last game added to assign GameId
            var games = GetAllGames() ?? new List<Game>();
            var lastGame = games.OrderByDescending(g => g.GameId).FirstOrDefault();
            game.GameId = lastGame?.GameId + 1 ?? 1;

            //Add game to list and write to xml file
            games.Add(game);
            WriteXml(games);

            //return game with Id assigned
            return game.GameId;
        }

        public void UpdateGame(Game game)
        {
            var games = GetAllGames();
            var gameToUpdate = games?.FirstOrDefault(g => g.GameId == game.GameId);

            if (gameToUpdate == null)
                return;

            gameToUpdate.GameName = game.GameName;
            gameToUpdate.GameDescription = game.GameDescription;


            WriteXml(games);
        }

        public List<Game> GetAllGames()
        {
            if (!File.Exists(XmlPath))
                return null;


            List<Game> games = null;
            var serializer = new XmlSerializer(typeof(List<Game>));
            using (var reader = XmlReader.Create(XmlPath))
            {
                games = (List<Game>)serializer.Deserialize(reader);
            }


            return games.OrderBy(g => g.GameId).ToList();
        }

        public Game GetGameById(int gameId)
        {
            return GetAllGames()?.FirstOrDefault(g => g.GameId == gameId);
        }

        public void DeleteGame(int gameId)
        {
            var games = GetAllGames();
            var gameToDelete = games?.FirstOrDefault(g => g.GameId == gameId);
            if (gameToDelete == null)
                return;

            games.Remove(gameToDelete);
            WriteXml(games);
        }

        /// <summary>
        /// Write games to xml file
        /// </summary>
        /// <param name="games">Games Collection</param>
        private void WriteXml(List<Game> games)
        {
            games = games.OrderBy(g => g.GameId).ToList();
            Directory.CreateDirectory(DirectoryPath);

            var serializer = new XmlSerializer(typeof (List<Game>));
            var xDocument = new XDocument();
            using (var xDocWriter = xDocument.CreateWriter())
            {
                serializer.Serialize(xDocWriter, games);
            }

            xDocument.Save(XmlPath);

        }

    }
}