﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoardGamesCollection.ValueObjects;

namespace BoardGamesCollection.Providers
{
    public interface IGameProvider
    {
        /// <summary>
        /// Inserts New Game
        /// </summary>
        /// <param name="game">Game to insert</param>
        /// <returns>GameId of game inserted</returns>
        int AddGame(Game game);

        /// <summary>
        /// Updates game
        /// </summary>
        /// <param name="game">game to be updated</param>
        void UpdateGame(Game game);

        /// <summary>
        /// Return a list with all games
        /// </summary>
        /// <returns>List with all games saved, returns null if data store not found</returns>
        List<Game> GetAllGames();

        /// <summary>
        /// Get Game with the given Id, return null if not found
        /// </summary>
        /// <param name="gameId">GameId</param>
        /// <returns>Game</returns>
        Game GetGameById(int gameId);

        /// <summary>
        /// Deletes game with the id specified
        /// </summary>
        /// <param name="gameId">GameId of game to be deleted</param>
        void DeleteGame(int gameId);
    }
}
