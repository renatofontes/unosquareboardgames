﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoardGamesCollection;
using BoardGamesCollection.Controllers;
using BoardGamesCollection.Providers;
using BoardGamesCollection.ValueObjects;
using Moq;

namespace BoardGamesCollection.Tests.Controllers
{
    [TestClass]
    public class GamesApiControllerTest
    {
        private readonly Mock<IGameProvider> _gameProvider = new Mock<IGameProvider>();

        [TestInitialize]
        public void Setup()
        {
            SetupMocks();
        }

        private void SetupMocks()
        {
            _gameProvider.Setup(g => g.GetAllGames()).Returns(new List<Game>()
            {
                new Game
                {
                    GameId = 1,
                    GameName = "Jenga",
                    GameDescription = "Game where you stack wooden pieces"
                },
                new Game
                {
                    GameId = 2,
                    GameName = "Chess",
                    GameDescription = "Strategy game"
                },
                new Game
                {
                    GameId = 3,
                    GameName = "Catan",
                    GameDescription = "Game where you build cities"
                }
            });

            _gameProvider.Setup(g => g.GetGameById(1)).Returns(new Game
            {
                GameId = 1,
                GameName = "Jenga",
                GameDescription = "Game where you stack wooden pieces"
            });

            _gameProvider.Setup(g => g.GetGameById(20)).Returns((Game) null);

            _gameProvider.Setup(g => g.AddGame(It.IsAny<Game>()))
                .Callback(() => _gameProvider.Setup(g => g.GetGameById(20)).Returns(new Game
                {
                    GameId = 20,
                    GameName = "Dungeons and Dragons",
                    GameDescription = "StrategyGame"
                }));
        }

        [TestMethod]
        public void Test_Get()
        {
            // Arrange
            var controller = new GamesApiController(_gameProvider.Object);

            // Act
            var result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());
        }

        [TestMethod]
        public void Test_GetById()
        {
            // Arrange
            var controller = new GamesApiController(_gameProvider.Object);

            // Act
            var result = controller.Get(1);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.GameId == 1);
            Assert.IsTrue(result.GameName == "Jenga");

            result = controller.Get(20);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void Test_PostAndGet()
        {
            // Arrange
            var controller = new GamesApiController(_gameProvider.Object);

            var getResult = controller.Get(20);
            Assert.IsNull(getResult);

            var postResult = controller.Post(new Game
            {
                GameName = "Dungeons and Dragons",
                GameDescription = "StrategyGame"
            });

            getResult = controller.Get(20);
            Assert.IsNotNull(getResult);
            Assert.IsTrue(getResult.GameId == 20);
            Assert.IsTrue(getResult.GameName == "Dungeons and Dragons");


        }

    }
}
