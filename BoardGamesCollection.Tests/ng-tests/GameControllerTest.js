﻿/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/ai.0.15.0-build58334.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/ai.0.15.0-build58334.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/jquery-1.10.2.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/jquery.validate-vsdoc.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/jquery.validate.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/jquery.validate.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/jquery.validate.unobtrusive.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/bootstrap.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/modernizr-2.6.2.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/respond.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-animate.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-aria.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-cookies.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-loader.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-message-format.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-messages.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-mocks.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-parse-ext.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-resource.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-route.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-sanitize.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-scenario.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Angular/angular-touch.min.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Jasmine/boot.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Jasmine/console.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Jasmine/jasmine-html.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/Jasmine/jasmine.js" />
/// <reference path="C:\Users\Renato\Documents\Visual Studio 2015\Projects\BoardGamesCollection\BoardGamesCollection\Scripts/ng-scripts/app.js" />


describe('Controller: GamesController', function () {
    beforeEach(module('app'));
    var createController, $httpBackend, $rootScope;
    beforeEach(inject(function ($injector) {
        var $controller = $injector.get('$controller');
        
        ctrl = $controller('GamesController', {});


    }));

    
    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });

    it('should have games defined', function () {
        expect(ctrl.games).toBeDefined();
    });

});

